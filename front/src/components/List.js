import React, { Component } from "react";
import TaskService from "../services";
import { Link } from "react-router-dom";

export default class List extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveTasks = this.retrieveTasks.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveTask = this.setActiveTask.bind(this);
    this.removeAllTasks = this.removeAllTasks.bind(this);

    this.state = {
      tasks: [],
      current: null,
      currentIndex: -1,
      searchTitle: "",

      page: 1,
      count: 0,
      pageSize: 3,
    };

    this.pageSizes = [3, 6, 9];
  }

  componentDidMount() {
    this.retrieveTasks();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  getRequestParams(searchTitle) {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    return params;
  }

  retrieveTasks() {
    const { searchTitle} = this.state;
    let params = {};
    if (searchTitle) {
      params["title"] = searchTitle;
    }

    TaskService.getAll(params)
      .then((response) => {
        console.log(response.data)
        this.setState({
          tasks: response.data
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveTasks();
    this.setState({
      current: null,
      currentIndex: -1,
    });
  }

  setActiveTask(task, index) {
    this.setState({
      current: task,
      currentIndex: index,
    });
  }

  removeAllTasks() {
    TaskService.deleteAll()
      .then((response) => {
        console.log(response.data);
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }


  render() {
    const {
      searchTitle,
      tasks,
      current,
      currentIndex
    } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Tasks List</h4>

          <ul className="list-group">
            
            {tasks &&
              tasks.map((task, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  style={{display: 'flex',
                    justifyContent: 'space-between', alignItems: 'center'}}
                  onClick={() => this.setActiveTask(task, index)}
                  key={index}
                >
                  <div>{task.title}</div>
                  <Link
                to={"/tasks/" + task._id}
                className="badge badge-warning"
              >
                Edit
              </Link>
                </li>
                
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllTasks}
          >
            DElete All
          </button>
        </div>
        <div className="col-md-6">
          {current ? (
            <div>
              <h4>Task</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>{" "}
                {current.title}
              </div>
              <div>
                <label>
                  <strong>Описание:</strong>
                </label>{" "}
                {current.description}
              </div>
              <div>
                <label>
                  <strong>Статус:</strong>
                </label>{" "}
                {current.published ? "Published" : "Pending"}
              </div>
            </div>
          ) : (
            <div>
            </div>
          )}
        </div>
      </div>
    );
  }
}