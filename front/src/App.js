import React, { useState, useEffect } from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import {Link, Switch, Route} from 'react-router-dom'
import Add from './components/Add'
import Task from './components/Task'
import List from './components/List'
function App() {
  return (
    <div>
    <nav className="navbar navbar-expand navbar-dark bg-dark">
      <div className="navbar-nav mr-auto">
        <li className="nav-item">
          <Link to={"/"} className="nav-link">Задачи</Link>
        </li>
        <li className="nav-item">
          <Link to={"/adding"} className="nav-link">Добавление</Link>
        </li>
      </div>
    </nav>

    <div className="container mt-3">
      <Switch>
        <Route exact path={["/", "/tasks"]} component={List} />
        <Route exact path="/adding" component={Add} />
        <Route path="/tasks/:id" component={Task} />
      </Switch>
    </div>
  </div>
  );
}
export default App;
