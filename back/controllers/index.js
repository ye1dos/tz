const db = require("../models");
const Tasks = db.tasks;

exports.create = (req, res) => {
  if (!req.body.title) {
    res.send({ message: "Content is empty" });
    return;
  }

  const task = new Tasks({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? true : false
  });

  task.save(task)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.send({
        message: "Error"
      });
    });
};

exports.findAll = (req, res) => {
  const title = req.query.title;
  
  Tasks.find()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.send({
        message: "Error"});
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Tasks.findById(id)
    .then(data => {
      if (!data)
        res.send({ message: "Not found" });
      else res.send(data);
    })
    .catch(err => {
      res.send({ message: "Error" });
    });
};

exports.update = (req, res) => {
  if (!req.body) {
    return res.send({
      message: "Data can not be empty!"
    });
  }

  const id = req.params.id;

  Tasks.findByIdAndUpdate(id, req.body)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: "Task not found"
        });
      } else res.send({ message: "Updated successfully." });
    })
    .catch(err => {
      res.status(500).send({message: "Error"});
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Tasks.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.send({
          message: "Error"});
      } else {
        res.send({
          message: "Deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error"
      });
    });
};

exports.deleteAll = (req, res) => {
  Tasks.deleteMany({})
    .then(data => {
      res.send({
        message: "Successfully deleted"
      });
    })
    .catch(err => {
      res.send({message: "Error with server"});
    });
};

exports.findAllPublished = (req, res) => {
  Tasks.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.send({
        message: "Error with server"
      });
    });
};