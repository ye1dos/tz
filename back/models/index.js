const conf = require("../config/config");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = { };
db.mongoose = mongoose;
db.url = conf.url;
db.tasks = require("./model")(mongoose);

module.exports = db;