const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsSettings = {
  origin: "http://localhost:3000"
};

app.use(cors(corsSettings));
app.use(bodyParser.json());

const db = require("./back/models");
db.mongoose.connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected!");
  })
  .catch(error => {
    console.log("Cannot connect", error);
    process.exit();
  });

app.get("/", (req, res) => {
  res.json({ message: "Welcome" });
});

require("./back/routes")(app);
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});